<?php

use yii\db\Schema;
use yii\db\Migration;

class m150121_182804_create_foreign_key_for_tag_table extends Migration
{
    public function up()
    {
	    $this->addForeignKey('site_id_FK_tag', 'tag', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150121_182804_create_foreign_key_for_tag_table cannot be reverted.\n";

        return false;
    }
}
