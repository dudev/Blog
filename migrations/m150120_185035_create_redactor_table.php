<?php

use yii\db\Schema;
use yii\db\Migration;

class m150120_185035_create_redactor_table extends Migration
{
    public function up()
    {
	    $this->createTable('redactor', [
		    'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'site_id' => Schema::TYPE_SMALLINT . ' NOT NULL',
	    ]);
	    $this->addPrimaryKey('redactor_tbl_primary_key', 'redactor', ['site_id', 'user_id']);
    }

    public function down()
    {
        echo "m150120_185035_create_redactor_table cannot be reverted.\n";

        return false;
    }
}
