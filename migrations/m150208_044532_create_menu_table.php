<?php

use yii\db\Schema;
use yii\db\Migration;

class m150208_044532_create_menu_table extends Migration
{
    public function up()
    {
	    $this->createTable('menu', [
		    'id' => Schema::TYPE_PK,
		    'name' => Schema::TYPE_STRING . '(50) NOT NULL',
		    'type' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'route' => Schema::TYPE_STRING . ' NOT NULL',
		    'number' => Schema::TYPE_SMALLINT . ' NOT NULL',
		    'site_id' => Schema::TYPE_SMALLINT . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('site_id_FK_menu', 'menu', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150208_044532_create_menu_table cannot be reverted.\n";

        return false;
    }
}