<?php

use yii\db\Schema;
use yii\db\Migration;

class m150121_182842_create_foreign_key_for_vk_site_table extends Migration
{
    public function up()
    {
	    $this->addForeignKey('site_id_FK_vk_site', 'vk_site', 'site_id', 'site', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150121_182842_create_foreign_key_for_vk_site_table cannot be reverted.\n";

        return false;
    }
}
