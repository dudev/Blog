<?php

use yii\db\Schema;
use yii\db\Migration;

class m150120_181807_create_tag_post_table extends Migration
{
    public function up()
    {
	    $this->createTable('tag_post', [
		    'tag_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'post_id' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addPrimaryKey('tag_post_tbl_primary_key', 'tag_post', ['tag_id', 'post_id']);
	    $this->addForeignKey('tag_FK_tag_post', 'tag_post', 'tag_id', 'tag', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('post_FK_tag_post', 'tag_post', 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150120_181807_create_tag_post_table cannot be reverted.\n";

        return false;
    }
}
