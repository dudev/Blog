<?php

use yii\db\Schema;
use yii\db\Migration;

class m150120_181844_create_site_table extends Migration
{
    public function up()
    {
	    $this->createTable('site', [
		    'id' => Schema::TYPE_SMALLINT . ' NOT NULL PRIMARY KEY AUTO_INCREMENT',
		    'name' => Schema::TYPE_STRING . ' NOT NULL',
		    'domain' => Schema::TYPE_STRING . ' NOT NULL',
		    'owner_id' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
    }

    public function down()
    {
        echo "m150120_181844_create_site_table cannot be reverted.\n";

        return false;
    }
}
