<?php

use yii\db\Schema;
use yii\db\Migration;

class m150710_132704_add_mainpage_field_in_site_tbl extends Migration
{
    public function up()
    {
	    $this->addColumn('site', 'mainpage', Schema::TYPE_STRING . '(20)');
    }

    public function down()
    {
        echo "m150710_132704_add_mainpage_field_in_site_tbl cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
