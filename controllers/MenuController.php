<?php

namespace app\controllers;

use app\extensions\Controller;
use Yii;
use app\models\Menu;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    public function behaviors() {
        return [
	        'access' => [
		        'class' => AccessControl::className(),
		        'only' => ['create', 'delete', 'update', 'admin'],
		        'rules' => [
			        [
				        'actions' => ['create', 'delete', 'update', 'admin'],
				        'allow' => true,
				        'matchCallback' => function ($rule, $action) {
					        /* @var \app\extensions\Controller $controller */
					        $controller = \Yii::$app->controller;
					        return $controller->isAdmin();
				        }
			        ],
		        ],
	        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionAdmin() {
        $dataProvider = new ActiveDataProvider([
            'query' => Menu::find()->orderBy(['type' => SORT_ASC, 'number' => SORT_ASC]),
	        'pagination' => [
		        'pageSize' => 15,
	        ],
        ]);

        return $this->render('admin', [
            'dataProvider' => $dataProvider,
        ]);
    }

	/**
	 * Creates a new Menu model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @throws Exception
	 * @throws \Exception
	 * @throws \yii\db\Exception
	 * @return mixed
	 */
    public function actionCreate() {
        $model = new Menu();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
	        $transaction = Yii::$app->db->beginTransaction();
	        try {
		        //опускаем пункты, если необходимо
		        (new Query())
			        ->createCommand()
			        ->update(
				        Menu::tableName(),
				        [
					        'number' => new Expression('number + 1')
				        ],
				        [
					        'and',
					        ['>=', 'number', $model->number],
					        ['site_id' => $this->site->id, 'type' => $model->type],
				        ]
			        )
		            ->execute();
		        $model->save();
		        $transaction->commit();
	        } catch(Exception $e) {
		        $transaction->rollBack();
		        throw $e;
	        }

            return $this->redirect(['admin']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

	/**
	 * Updates an existing Menu model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @throws Exception
	 * @throws NotFoundHttpException
	 * @throws \Exception
	 * @throws \yii\db\Exception
	 * @return mixed
	 */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
	        $transaction = Yii::$app->db->beginTransaction();
	        try {
		        if($model->getOldAttribute('number') != $model->number) {
			        (new Query())
				        ->createCommand()
				        ->update(
					        'menu', [
						        'number' => new Expression('number - 1')
					        ],
					        [
						        'and',
						        ['>', 'number', $model->getOldAttribute('number')],
						        ['site_id' => $this->site->id, 'type' => $model->type],
					        ]
				        )
				        ->execute();
			        (new Query())
				        ->createCommand()
				        ->update(
					        'menu', [
						        'number' => new Expression('number + 1')
					        ],
					        [
						        'and',
						        ['>=', 'number', $model->number],
						        ['site_id' => $this->site->id, 'type' => $model->type],
					        ]
				        )
				        ->execute();
		        }
		        $model->save();
		        $transaction->commit();
	        } catch(Exception $e) {
		        $transaction->rollBack();
		        throw $e;
	        }
            return $this->redirect(['admin']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

	/**
	 * Deletes an existing Menu model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @throws Exception
	 * @throws NotFoundHttpException
	 * @throws \Exception
	 * @throws \yii\db\Exception
	 * @return mixed
	 */
    public function actionDelete($id) {
        $model = $this->findModel($id);
	    $transaction = Yii::$app->db->beginTransaction();
	    try {
		    (new Query())
			    ->createCommand()
			    ->update(
				    'menu', [
					    'number' => new Expression('number - 1')
				    ],
				    [
					    'and',
					    ['>', 'number', $model->number],
					    ['site_id' => $this->site->id, 'type' => $model->type],
				    ]
			    )
			    ->execute();
		    $model->delete();
		    $transaction->commit();
	    } catch(Exception $e) {
		    $transaction->rollBack();
		    throw $e;
	    }
        return $this->redirect(['admin']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if ($model = Menu::findOne($id)) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
