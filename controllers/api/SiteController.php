<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\Site;

class SiteController extends ApiController {
	public $defaultAction = 'list';
	protected $_except_action_check_domain = ['create', 'list', 'is-set'];
	public function actionCreate() {
		if(\Yii::$app->request->isGet) {
			return $this->sendError(self::ERROR_ILLEGAL_REQUEST_METHOD);
		}
		$model = new Site();
		$model->loadDefaultValues();

		if(!$model->load(\Yii::$app->request->post())) {
			return $this->sendError(self::ERROR_NO_DATA);
		}

		if($model->validate()) {
			if(!$model->save(false)) {
				return $this->sendError(self::ERROR_DB);
			}
			return $this->sendSuccess([
				'site' => [
					'id' => $model->id,
					'name' => $model->name,
					'domain' => $model->domain,
				]
			]);
		} else {
			$error_codes = [
				'name' => self::ERROR_ILLEGAL_SITE_NAME,
				'domain' => self::ERROR_ILLEGAL_SITE_DOMAIN,
			];
			foreach ($model->getFirstErrors() as $attr => $err) {
				if(isset($error_codes[ $attr ])) {
					$errors[] = $error_codes[ $attr ];
				}
			}
		}

		if(!isset($errors)) {
			$errors = self::ERROR_UNKNOWN;
		}
		return $this->sendError($errors);
	}
	public function actionUpdate() {
		if(\Yii::$app->request->isPost) {
			if (!$this->site->load(\Yii::$app->request->post())) {
				return $this->sendError(self::ERROR_NO_DATA);
			}

			if ($this->site->validate()) {
				if (!$this->site->save(false)) {
					return $this->sendError(self::ERROR_DB);
				}
			} else {
				$error_codes = [
					'name' => self::ERROR_ILLEGAL_SITE_NAME,
					'domain' => self::ERROR_ILLEGAL_SITE_DOMAIN,
				];
				foreach ($this->site->getFirstErrors() as $attr => $err) {
					if (isset($error_codes[$attr])) {
						$errors[] = $error_codes[$attr];
					}
				}
				if(!isset($errors)) {
					$errors = self::ERROR_UNKNOWN;
				}
				return $this->sendError($errors);
			}
		}

		return $this->sendSuccess([
			'site' => [
				'id' => $this->site->id,
				'name' => $this->site->name,
				'domain' => $this->site->domain,
			]
		]);
	}
	public function actionDelete() {
		if($this->site->delete() === false) {
			return $this->sendError(self::ERROR_DB);
		} else {
			return $this->sendSuccess([]);
		}
	}
	public function actionIsSet($id) {
		if(Site::findOne($id)) {
			return $this->sendSuccess([]);
		}
		return $this->sendError(self::ERROR_NO_SITE);
	}
    public function actionList() {
	    $res = Site::find()
		    ->select('id, name, domain')
		    ->asArray()
		    ->all();

	    if($res === []) {
		    return $this->sendError(self::ERROR_DB);
	    }

	    return $this->sendSuccess(['sites' => $res]);
    }
}
