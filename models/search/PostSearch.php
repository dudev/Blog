<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\models\search;


use app\models\Post;
use yii\data\ActiveDataProvider;

class PostSearch extends Post {
	public function rules() {
		// only fields in rules() are searchable
		return [
			[['id', 'status'], 'integer'],
		];
	}

	public function search($params, $site_id) {
		$query = Post::find()
			->select(['id', 'title', 'publish_at', 'status'])
			->where(['site_id' => $site_id]) ;

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 15,
			],
		]);

		// load the search form data and validate
		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		// adjust the query by adding the filters
		$query->andFilterWhere(['id' => $this->id]);
		if($this->status == Post::STATUS_WAITING) {
			$query->andFilterWhere(['>', 'publish_at', time()])
				->andFilterWhere(['status' => Post::STATUS_WAITING]);
		} else {
			$query->andFilterWhere(['status' => $this->status]);
		}

		return $dataProvider;
	}
}