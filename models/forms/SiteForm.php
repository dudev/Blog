<?php
/**
 * Project: blog
 * User: Sergey Bessonov
 * E-mail: sergebessonov@gmail.com
 */

namespace app\models\forms;


use app\models\Site;

class SiteForm extends Site {

	public function rules() {
		return [
			[['name'], 'required'],
			[ 'name', 'string', 'max' => 255]
		];
	}

} 