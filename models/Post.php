<?php

namespace app\models;

use general\ext\Helper;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\imagine\Image;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $nick
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $text
 * @property integer $status
 * @property boolean $picture
 * @property boolean $pic_in_post
 * @property integer $site_id
 * @property integer $publish_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 *
 * @property User $author
 * @property Site $site
 * @property TagPost[] $tagPosts
 * @property Tag[] $tags
 * @property array $marks
 * @property \yii\web\UploadedFile $image
 */
class Post extends ActiveRecord {
	const STATUS_DRAFT = 0;
	const STATUS_PUBLISHED = 1;
	const STATUS_WAITING = 2;

	public static $statuses = [
		self::STATUS_DRAFT => 'Черновик',
		self::STATUS_PUBLISHED => 'Опубликовано',
		self::STATUS_WAITING => 'Ожидает',
	];

	public $image;
	public $image_is_uploaded = false;

	private $_marks;

	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
			['marks', 'required', 'when' => function($model) {
				return (bool)$model->status;
			}, 'whenClient' => 'function (attribute, value) {
		        return $("#post-status").val() == "' . Post::STATUS_PUBLISHED . '";
		    }'],
			['title', 'trim'],
			['text', 'string'],
	        ['nick', 'filter', 'filter' => function ($value) {
		        if(!$value) {
			        return Helper::translit($this->title);
		        }
		        return $value;
	        }],
			[['status', 'picture', 'pic_in_post'], 'boolean'],
	        ['publish_at', 'filter', 'filter' => function($value) {
		        $parsed = strtotime($value);
		        if(!$parsed) {
			        return time();
		        }
		        return $parsed;
	        }],
			[['nick', 'title'], 'string', 'max' => 100],
	        ['keywords', 'filter', 'filter' => function($value) {
		        if(!$value) {
			        return Helper::cutter($this->marks, 128);
		        }
		        return $value;
	        }],
			['keywords', 'string', 'max' => 128],
	        ['description', 'filter', 'filter' => function($value) {
		        if(!$value) {
			        return Helper::cutter($this->text, 250);
		        }
		        return $value;
	        }],
			['description', 'string', 'max' => 250],
	        [
		        'image',
		        'file',
		        'skipOnEmpty' => false,
		        'when' => function($model) {
			        /* @var $model Post */
			        return (bool)$model->picture && !$model->image_is_uploaded;
		        },
		        'whenClient' => 'function (attribute, value) {
			        return $("#post-picture").prop("checked") && ' . (!$this->image_is_uploaded ? 'true' : 'false') . ';
			    }',
		        'uploadRequired' => 'Необходимо прикрепить картинку.',
	        ],
	        [
		        'image',
		        'file',
		        'extensions' => 'jpg, jpeg, gif',
		        'mimeTypes' => 'image/jpeg, image/gif',
	        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nick' => 'Мнемоника',
            'title' => 'Название',
            'keywords' => 'Ключевые слова',
            'description' => 'Описание',
            'text' => 'Текст',
            'status' => 'Статус',
            'picture' => 'Картинка',
            'pic_in_post' => 'Показывать картинку внутри статьи?',
            'publish_at' => 'Дата публикации',
            'author_id' => 'Автор',
	        'marks' => 'Метки',
	        'image' => 'Картинка',
        ];
    }

	public function beforeSave($insert) {
		if(parent::beforeSave($insert)) {
			if ($this->isNewRecord) {
				$this->author_id = Yii::$app->user->id;
				$this->site_id = Yii::$app->controller->site->id;
			}
			return true;
		}
		return false;
	}

	public function afterFind() {
		parent::afterFind();
		if($this->picture) {
			$this->image_is_uploaded = true;
		}
	}

	public function getMarks() {
		if($this->_marks) {
			return $this->_marks;
		}
		if(!$this->isNewRecord) {
			$tags = [];
			foreach ($this->tags as $tag) {
				$tags[] = $tag->name;
			}
			return $this->_marks = implode(', ', $tags);
		}
		return '';
	}

	public function setMarks($val) {
		$this->_marks = $val;
	}

	public function saveImage() {
		if($this->image) {
			if($this->image_is_uploaded) {
				if(file_exists(\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '.jpg')) {
					unlink(\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '.jpg');
				}
				if(file_exists(\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '-big.jpg')) {
					unlink(\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '-big.jpg');
				}
				if(file_exists(\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '-small.jpg')) {
					unlink(\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '-small.jpg');
				}
			}

			if(!file_exists(\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id)) {
				mkdir(\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id, 0750, true);
			}

			$this->image->saveAs(\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '.jpg');
			//формируем большую картинку
			Image::thumbnail(
				\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '.jpg',
				640,
				640
			)->save(
				\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '-big.jpg',
				['quality' => 90]
			);
			//формируем маленькую картинку
			Image::thumbnail(
				\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '-big.jpg',
				320,
				320
			)->save(
				\Yii::getAlias('@app/web/i/post') . '/' . $this->site_id . '/' . $this->id . '-small.jpg',
				['quality' => 70]
			);
		}
	}

	public static function getImageLinks($model, $types = ['source', 'big', 'small']) {
		$ret = [];
		foreach ((array)$types as $type) {
			$ret[$type] = '//' . Yii::$app->request->serverName . '/i/post/' . $model['site_id'] . '/' . $model['id'];
			if($type == 'source') {
				$ret[$type] .= '.jpg';
			} else {
				$ret[$type] .= '-' . $type . '.jpg';
			}
		}
		return $ret;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagPosts()
    {
        return $this->hasMany(TagPost::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('tag_post', ['post_id' => 'id']);
    }
}
