<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $route
 * @property integer $number
 * @property integer $site_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Site $site
 */
class Menu extends ActiveRecord {
	public $category;
	public $link;

	const TYPE_CATEGORY = 1;
	const TYPE_MENU = 2;

	public static $types = [
		self::TYPE_CATEGORY => 'Категории',
		self::TYPE_MENU => 'Меню',
	];

	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
	        [['name', 'type', 'number'], 'required'],
	        [
		        'category',
		        'required',
		        'when' => function($model) {
			        return $model->type == Menu::TYPE_CATEGORY;
		        },
		        'whenClient' => 'function (attribute, value) {
			        return $("#menu-type").val() == "' . Menu::TYPE_CATEGORY . '";
			    }',
	        ],
	        [
		        'link',
		        'required',
		        'when' => function($model) {
			        return $model->type == Menu::TYPE_MENU;
		        },
		        'whenClient' => 'function (attribute, value) {
			        return $("#menu-type").val() == "' . Menu::TYPE_MENU . '";
			    }',
	        ],
	        [['number', 'created_at', 'updated_at'], 'integer'],
	        [['type'], 'in', 'range' => [
		        Menu::TYPE_CATEGORY,
		        Menu::TYPE_MENU,
	        ]],
	        [['name'], 'string', 'max' => 50],
	        [
		        'category',
		        'exist',
		        'targetClass' => Tag::className(),
		        'targetAttribute' => 'name',
		        'filter' => [
			        'site_id' => \Yii::$app->controller->site->id,
		        ],
	        ],
	        [['link'], 'string', 'max' => 255],
            [['route'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'type' => 'Тип',
            'route' => 'Ссылка',
            'number' => '#',
	        'category' => 'Категория',
	        'link' => 'Ссылка',
        ];
    }

	public function beforeSave($insert) {
		if(parent::beforeSave($insert)) {
			if ($this->isNewRecord) {
				$this->site_id = Yii::$app->controller->site->id;
			}
			switch($this->type) {
				case self::TYPE_CATEGORY:
					$this->route = json_encode(['post/list', 'tag' => $this->category]);
					break;
				case self::TYPE_MENU:
					$this->route = json_encode($this->link);
					break;
			}
			return true;
		}
		return false;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }
}
