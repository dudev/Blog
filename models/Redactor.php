<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "redactor".
 *
 * @property integer $user_id
 * @property integer $site_id
 *
 * @property Site $site
 * @property User $user
 */
class Redactor extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'redactor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'required'],
            ['user_id', 'integer'],
	        ['user_id', 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'id'],
	        ['user_id', 'validateUserId']
        ];
    }
	public function validateUserId($attribute)
	{
		if (Redactor::findOne(['user_id' => $this->user_id, 'site_id' => Yii::$app->controller->site->id])) {
			$this->addError($attribute, 'Редактор с таким ID уже добавлен.');
		}
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user.nick' => 'Имя Пользователя',
	        'user_id' => 'ID Пользователя',
            'site_id' => 'Блог',
        ];
    }

	public function beforeSave($insert) {
		if(parent::beforeSave($insert)) {
			if ($this->isNewRecord) {
				$this->site_id = Yii::$app->controller->site->id;
			}
			return true;
		}
		return false;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
