<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\models\api\forms;


use app\models\Tag;

class TagForm extends Tag {
	public function rules() {
		return [
			['name', 'required'],
			['name', 'string', 'max' => 50]
		];
	}
} 