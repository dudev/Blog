<?php
use yii\helpers\Html;

/* @var $data [] */
/* @var $this yii\web\View */

$this->title = $data['title'];
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
	'name' => 'description',
	'content' => $data['description'],
], 'description');
$this->registerMetaTag([
	'name' => 'keywords',
	'content' => $data['keywords'],
], 'keywords');

if(!empty($data['template'])) {
	echo $data['template']['head'], $data['template']['body_start'], $data['text'], $data['template']['body_end'];
} else {
	echo $data['text'];
}