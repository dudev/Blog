<?php
/**
 * Project: Auth - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
use general\ext\api\ApiController;

/* @var $retUrl string */
/* @var $result string */
/* @var $data string|array */

$url = $retUrl . '?result=' . $result;
switch($result) {
	case 'error':
		$url .= '&errors[0][code]=' . $data . '&errors[0][text]=' . ApiController::$_errors[ $data ];
		break;
	case 'success':
		foreach($data as $key => $value) {
			$url .= '&' . $key . '=' . $value;
		}
		break;
}
?>
<script type="text/javascript">
parent.location.href= "<?=$url ?>";
</script>