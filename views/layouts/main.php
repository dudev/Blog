<?php
use app\models\Menu;
use general\widgets\MonoMenu;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

/* @var $controller app\extensions\Controller */
$controller = Yii::$app->controller;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="zQMN8ZleT4eOxtLqnjrHQekcmIvjCJKWjm1eWKhtagE" />
    <?= Html::csrfMetaTags() ?>
    <title><?= $this->title ? Html::encode($this->title) . ' - ' : '' ?><?= $controller->site->name ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<header>
	<a href="/" title="Главная - <?= $controller->site->name ?>"><?= $controller->site->name ?></a>
</header>
<div id="page">
	<?= Breadcrumbs::widget([
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		'homeLink'=>[
			'label' => 'Главная',
			'url' => Yii::$app->homeUrl,
		],
		'itemTemplate' => '<li>{link} » </li>',
	]) ?>
	<div id="content">
		<?= $content ?>
	</div>
	<div id="sidebar">
		<nav>
			<h4>Меню</h4>
			<?= MonoMenu::widget([
				'items' => Menu::find()->where(['type' => Menu::TYPE_MENU])->all(),
			]) ?>
			<? /* @todo ['label' => 'Обо мне', 'url' => '/about-me.html'],
			@todo ['label' => 'Услуги', 'url' => '/offers.html'],
			@todo ['label' => 'Контакты', 'url' => '/contacts.html'], */ ?>
		</nav>
		<nav>
			<h4>Категории</h4>
			<?= MonoMenu::widget([
				'items' => Menu::find()->where(['type' => Menu::TYPE_CATEGORY])->all(),
			]) ?>
		</nav>
		<?php
		/* @var \app\extensions\Controller $controller */
		$controller = Yii::$app->controller;
		if(!Yii::$app->user->isGuest && $controller->isAdmin()) {
			if(Yii::$app->requestedRoute == 'post/view') {
				echo Html::beginTag('nav');
				echo Html::tag('h4', 'Управление записью');
				echo \yii\widgets\Menu::widget([
					'items' => [
						['label' => 'Изменить', 'url' => ['post/update', 'id' => Yii::$app->request->get('id')]],
					]
				]);
				echo Html::endTag('nav');
			}
			echo Html::beginTag('nav');
			echo Html::tag('h4', 'Управление');
			echo \yii\widgets\Menu::widget([
				'items' => [
					['label' => 'Создать запись', 'url' => ['post/create']],
					['label' => 'Записи', 'url' => ['post/admin']],
					//['label' => 'Комментарии', 'url' => ['comment/index']],
					['label' => 'Меню', 'url' => ['menu/admin']],
					['label' => 'Страницы', 'url' => ['page/index']],
					['label' => 'Редакторы', 'url' => ['redactor/index']],
					['label' => 'Настройки сайта', 'url' => ['site/update']],
					['label' => 'Выход', 'url' => ['site/logout']],
				]
			]);
			echo Html::endTag('nav');
		} ?>

		<?php
		/*<h4>Поиск</h4>
		<form action="#" class="s">
			<input id="search" type="text" value="Я ищу...">
		</form>*/
		?>

		<?php //$this->widget('TagCloud'); ?>

</div>
<footer>
	<p>&copy;&nbsp;2008 -  <?= date('Y') ?>&nbsp;Евгений Русанов</p>
</footer>
</div>
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter21505294 = new Ya.Metrika({id:21505294, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/21505294" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
