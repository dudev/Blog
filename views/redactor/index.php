<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use general\assets\AdminAsset;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

AdminAsset::register($this);

$this->title = 'Управление редакоторами блога';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redactor-index">
    <p>
        <?= Html::a('Создать редактора', ['create']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
	    'layout' => '{items}{pager}',
        'columns' => [

            'user.nick',

	        [
		        'class' => ActionColumn::className(),
		        'template' => '{delete}'
	        ],
        ],
    ]); ?>

</div>
































