<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Redactor */


$this->title = 'Создать редактора';
$this->params['breadcrumbs'][] = ['label' => 'Редакторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form">

    <h1><?= Html::encode($this->title) ?></h1>

	<?php $form = ActiveForm::begin([
		'fieldConfig' => [
			'template' => '<div class="row">{label}{input}{error}</div>',
		],
	]); ?>

	<?= $form->field($model, 'user_id')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
