<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $tag string */
use yii\widgets\ListView;

if($tag) { ?>
	<h1>Записи с тегом <i><?= $tag ?></i></h1>
	<? $this->title = 'Записи с тегом ' . $tag;
} else { ?>
	<h1>Записи</h1>
<? }
$this->params['breadcrumbs'][] = 'Записи';

echo ListView::widget([
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
	'layout' => '{items}{pager}',
]);