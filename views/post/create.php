<?php
/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = 'Создать пост';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="post-create">
    <h1><?= $this->title ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
